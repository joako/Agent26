﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
	public static GameManager Instancia;
	public bool defeat;
	public float Objetivo;
	public float ObjetivoB;


	void Awake(){
		
		if (Instancia == null) {

			Instancia = this;
		
			DontDestroyOnLoad (gameObject);
		} 
		else {

			Destroy (gameObject);
		}

	}
	void Update (){
		if (defeat == true){
			SceneManager.LoadScene ("derrota");
			defeat = false;
		}
		if(ObjetivoB==22){
			SceneManager.LoadScene ("Victoria");
			ObjetivoB = 0;
		}
	}

}