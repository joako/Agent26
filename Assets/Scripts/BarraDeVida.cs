﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour {

	public Image vida;

	private Player personaje;

	// Use this for initialization
	void Start () 
	{ 
		personaje = FindObjectOfType<Player> ();
	}

	// Update is called once per frame
	void Update () {
		if (personaje != null)
		{
			float porcentaje = (personaje.vida * 100) / personaje.vidaInicial;
			vida.fillAmount = porcentaje / 100;

		} 
		else 
		{
			gameObject.SetActive (true);
		}
	}

}
