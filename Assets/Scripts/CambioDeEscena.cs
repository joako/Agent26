﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CambioDeEscena : MonoBehaviour {


	public void IrAEscena(string nombreEscena)
	{
		
		SceneManager.LoadScene (nombreEscena);
	}
}
