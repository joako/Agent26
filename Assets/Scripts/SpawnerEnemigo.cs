﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemigo : MonoBehaviour {


	public GameObject enemy;
	public float ratio  = 1;
	public float duration = 10;
	public float delayInicial = 1;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawnear", delayInicial, ratio);

		}

	// Update is called once per frame
	void Spawnear () 
	{
		if(GameManager.Instancia.Objetivo==3){
		Instantiate (enemy, transform.position, transform.rotation);	
		}
	}




}
