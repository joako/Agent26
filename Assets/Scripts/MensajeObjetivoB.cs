﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MensajeObjetivoB : MonoBehaviour {

	public GameObject enemy;
	public float ratio  =1000;
	public float duration = 3;
	public float delayInicial = 1;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawnear", delayInicial, ratio);
		Invoke ("Destruir", duration + delayInicial);
	}

	// Update is called once per frame
	void Spawnear () 
	{
		if (GameManager.Instancia.Objetivo == 3) {
			Instantiate (enemy, transform.position, transform.rotation);	
			}
	}

	void Destruir()
	{
		Destroy (gameObject);

	}
}
