﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class movNavMesh : MonoBehaviour {


	public Transform[] posicionesAPatrullar;
	public float velocidad = 4f;


	public bool patrullando = true;
	public bool siguiendo = true;


	public float rangoDeteccion = 5f;

	public float rangoAtaque = 1.5f;


	int index = 0;
	Animator animator;
	NavMeshAgent agent;
	Player personaje;

	void Start(){
		animator = GetComponentInChildren<Animator>(); 
		agent = GetComponent<NavMeshAgent> ();
		personaje = FindObjectOfType<Player> ();

		agent.speed = velocidad;
		agent.stoppingDistance = rangoAtaque;
	}

	void FixedUpdate () {

		if (personaje == null) return;

		float distanciaAlPersonaje = Vector3.Distance (transform.position, personaje.transform.position);


		if (distanciaAlPersonaje > rangoDeteccion){
			patrullando = true;
			siguiendo = false;
		}

		else{
			siguiendo = true;
			patrullando = false;
		}

	

		if (patrullando == true && posicionesAPatrullar.Length > 0){
			print ("Patrullando");

			animator.SetBool ("forward", true);
			animator.SetBool ("attack", false);
			float distanciaAlPunto = Vector3.Distance (	posicionesAPatrullar [index].position, 
				transform.position);


			if (distanciaAlPunto > agent.stoppingDistance)
			{
				agent.isStopped = false;
				agent.destination = posicionesAPatrullar [index].position;

			}

			else if (index < posicionesAPatrullar.Length - 1 ) {
				index++;
			}

			else{
				index = 0;
			}
	

		}


		if (siguiendo == true){



			if (distanciaAlPersonaje > rangoAtaque){

				print ("Siguiendo");
				agent.isStopped = false;
				agent.destination = personaje.transform.position;
			}

			else{
				agent.isStopped = true;
				print ("Atacando");
				animator.SetBool ("forward", false);
				animator.SetBool ("attack", true);
				transform.LookAt (personaje.transform);
			}

		}

	}

	void OnDrawGizmos(){

		Gizmos.color = Color.red;

		Gizmos.DrawWireSphere (transform.position, rangoDeteccion);
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, rangoAtaque);
	}
}
