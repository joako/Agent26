﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MensajeObjetivoA : MonoBehaviour {


	public float duration = 4;
	public float delayInicial = 1;
	// Use this for initialization
	void Start () {
		Invoke ("Destruir", duration + delayInicial);
	}

	// Update is called once per frame
	void Update () {
		
	}
	void Destruir()
	{
		Destroy (gameObject);

	}
}
