﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoBalaPlayer : MonoBehaviour {

	public float danio;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter (Collider pipo) {
		if (pipo.gameObject.tag == "enemigo"){
			VidaEnemigo vida = pipo.gameObject.GetComponent <VidaEnemigo> ();
			vida.VidaE -= danio;

			Destroy (gameObject); 
		}
	}
}
