﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovimientoPlayerFPS : MonoBehaviour {

	public float velocidadFrontal = 5;
	public float velocidadLateral = 5;
	public float velocidadEspalda = 3;
	public int multiplicadorCorrida = 2;


	private int multiplicadorInterno = 1;


	void Start () {
		

	}

	void FixedUpdate () 
	{
		
		if (Input.GetKey (KeyCode.LeftShift)){

			multiplicadorInterno = multiplicadorCorrida;
		} 
		else if (Input.GetKeyUp (KeyCode.LeftShift)){
			
			multiplicadorInterno = 1;
		}

		if (Input.GetKey (KeyCode.W)){

			transform.Translate (0f, 0f, velocidadFrontal * Time.deltaTime * multiplicadorInterno);
		}

		if (Input.GetKey (KeyCode.S)){
			transform.Translate (0f,0f, -velocidadEspalda * Time.deltaTime * multiplicadorInterno);
		}

		if (Input.GetKey (KeyCode.A)){
			transform.Translate (-velocidadLateral * Time.deltaTime * multiplicadorInterno,0f , 0f);
		}

		if (Input.GetKey (KeyCode.D)){
			transform.Translate (velocidadLateral * Time.deltaTime * multiplicadorInterno,0f, 0f);
		}


	}
		
		
}
